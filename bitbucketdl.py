#!/usr/bin/env python2


import web
import requests
import re
from urllib import quote
try:
	from cStringIO import StringIO
except:
	from StringIO import StringIO
import zipfile
import os
import sys
import shutil
import time
try:
	import android
	from multiprocessing import Process
	droid = android.Android()
except:
	android = None
import traceback







class Bitbucket:
	def __init__(self, username, password=None):
		self.username = username
		self.password = password
		self.session = requests.session(headers = {
			'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.11 (KHTML, like Gecko) Ubuntu/12.04 Chromium/20.0.1132.47 Chrome/20.0.1132.47 Safari/536.11'
		})
		if self.username != None and self.password != None:
			self.login()
	
	def login(self):
		r = self.session.get("https://bitbucket.org/account/signin/")
		html = r.text
		
		m = re.search("csrfmiddlewaretoken['\"][^>]*value=['\"]([^'\"]+)['\"]", html, re.S)
		if m:
			token = m.group(1)
		data = {
			'next': '/',
			'username': self.username,
			'password': self.password,
			'csrfmiddlewaretoken': token
		}
		r = self.session.post("https://bitbucket.org/account/signin/", headers={
			'Referer': 'https://bitbucket.org/account/signin/'
		},data=data)
		
		print "logged into bitbucket"
	
	def dlRepositoryFromUser(self, username, repository, branch=None):
		if branch == None:
			branch = "master"
		url = "https://bitbucket.org/%s/%s/get/%s.zip" % (quote(username), quote(repository), quote(branch))
		r = self.session.get(url)
		return r.content
		
	def dlRepository(self, repository, branch=None):
		return self.dlRepositoryFromUser(self.username, repository, branch)
		
	def extractTo(self, zipf, name):
		scriptDir = os.path.dirname(os.path.abspath(sys.argv[0]))
		extractDir = os.path.dirname(scriptDir)
		if name == "":
			name = "repository"
		dest = os.path.join( extractDir, name )
		
		z = zipfile.ZipFile(StringIO(zipf), 'r')
		for m in z.namelist():
			filename = m[m.find('/')+1:]
			if m.endswith('/'):
				destdir = os.path.abspath(os.path.join(dest, filename))
				try:
					os.makedirs(destdir)
					print "makedirs %s" % destdir
				except:
					pass
			else:
				destdir = os.path.dirname(os.path.abspath(os.path.join(dest, filename)))
				print "extract %s to %s" % (os.path.basename(m), destdir)
				source = z.open(m)
				target = file(os.path.join(destdir, os.path.basename(m)), "wb")
				shutil.copyfileobj(source, target)
				source.close()
				target.close()				
		
		z.close()
		


urls = (
	'/', 'Index',
	'/dlAndExtract', 'DlAndExtract',
)

# change working dir to the script dir
scriptdir = os.path.dirname(os.path.abspath(sys.argv[0]))
if os.path.exists(scriptdir):
	print "cd to %s" % os.path.dirname(os.path.abspath(sys.argv[0]))
	os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

#path = os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]))) 
#render = web.template.render(os.path.join(path, 'templates/'))
render = web.template.render("templates/")

class Index:
	def GET(self):
		try:
			c = web.cookies()
			username = c.get("bbdeploy_username")
			repository = c.get("bbdeploy_repository")
			branch = c.get("bbdeploy_branch")
		except:
			pass
		if username == None:
			username = "andrenam"
		if repository == None:
			repository = ""
		if branch == None:
			branch = "master"
		return render.index(username, repository, branch)

class DlAndExtract:
	def GET(self):
		try:
			i = web.input()
			username = i.username.strip()
			password = i.password.strip()
			if len(password) == 0:
				password = None
			repository = i.repository.strip()
			branch = i.branch.strip()
			bb = Bitbucket(username, password)
			dest = repository
			bb.extractTo(bb.dlRepository(repository, branch), dest)
			print "extracted Zipfile"
			
			# store last settings in Cookies
			web.setcookie('bbdeploy_username', username, 3600*24*60)
			web.setcookie('bbdeploy_repository', repository, 3600*24*60)
			web.setcookie('bbdeploy_branch', branch, 3600*24*60)
			
			return "ok"
		except:
			print "Fehler"
			error = traceback.format_exc()
			print error
			return error

class MyApplication(web.application): 
	def run(self, port=8080, *middleware): 
		func = self.wsgifunc(*middleware) 
		return web.httpserver.runsimple(func, ('0.0.0.0', port)) 


if __name__ == '__main__':
	if android != None:
		def showurl(url="http://localhost:9000"):
			time.sleep(5)
			droid.webViewShow(url)
		#p=Process(target=showurl)
		#p.start()
	#app = web.application(urls, globals())
	app = MyApplication(urls, globals()) 
	app.run(port=9000)
